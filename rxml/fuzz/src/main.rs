#[macro_use]
extern crate afl;
extern crate rxml;

use rxml::{error::EndOrError, Parse};

fn lex_chunked(chunks: &[&[u8]]) -> rxml::Result<usize> {
	let mut nevents = 0;
	let mut parser = rxml::Parser::new();
	parser.set_text_buffering(false);

	for chunk in chunks {
		let mut chunk = *chunk;
		while chunk.len() > 0 {
			match parser.parse_all(&mut chunk, false, |_| nevents += 1) {
				Err(EndOrError::NeedMoreData) => (),
				Err(EndOrError::Error(e)) => return Err(e),
				Ok(()) => panic!("eof reached before eof"),
			}
		}
	}

	match parser.parse_all(&mut &[][..], true, |_| nevents += 1) {
		Ok(()) => (),
		Err(EndOrError::NeedMoreData) => unreachable!(),
		Err(EndOrError::Error(e)) => return Err(e),
	}
	Ok(nevents)
}

fn main() {
	fuzz!(|data: &[u8]| {
		let mut had_any_err = false;
		let mut had_all_err = true;
		let mut chunks = Vec::<&[u8]>::new();
		let zero = &b"\0"[..];
		for chunk in data.split(|b| *b == b'\0') {
			if chunk.len() == 0 {
				chunks.push(zero)
			} else {
				chunks.push(chunk)
			}
		}
		match lex_chunked(&chunks) {
			Ok(_) => {
				had_all_err = false;
			}
			Err(_) => {
				had_any_err = true;
			}
		}
		let buf = chunks.join(&b""[..]);
		match lex_chunked(&[&buf]) {
			Ok(_) => {
				had_all_err = false;
			}
			Err(_) => {
				had_any_err = true;
			}
		}

		if had_any_err && !had_all_err {
			panic!("error state depends on chunking")
		}
	});
}
