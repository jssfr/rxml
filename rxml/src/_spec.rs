#![cfg(doc)]
/*!
# Restricted XML 1.0-1

Restricted XML is based on [XML 1.0 (Fifth Edition)][XML] and [Namespaces in XML 1.0 (Third edition)][XML-NAMES]. As the name suggests, the features offered by Restricted XML are a subset of the features offered by those standards. This document details exactly which restrictions apply for Restricted XML.

All Restricted XML documents are well-formed XML documents, but not vice versa.

Violations of criteria in sentences or paragraphs marked with `(RX)` MUST result in an error indicating violation of the Restricted XML restrictions. In other cases, it is possible that a generic syntax error is emitted.

## Grammar changes

- The `Comment` production and all references to it are deleted. Encountering the unescaped `<!--` sequence in places where the `Comment` production was previously referenced MAY raise a Restricted XML error.
- The `PI` production and all references to it are deleted. Encountering the unescaped `<?` sequence in places where the `PI` production was previously referenced MAY raise a Restricted XML error.
- Consequently, the `Misc` production becomes equivalent to `S` (whitespace).
- The `doctypedecl` production and all references to it are deleted.
- To avoid ambuguities, the `prolog` production is rewritten to:

	```text
	prolog ::= XMLDecl? S*
	```

	Without this explicit rewrite, the right-hand side would now be `XMLDecl? Misc* (Misc*)?`, which is nonsensical and confusing at best, and exponential at worst.

## Well-formedness changes

- The `standalone` attribute on the XML declaration MUST be `yes` or absent. (RX)
- The `encoding` attribute on the XML declaration MUST be `utf-8` or absent. (RX)

## Violations

- §2.2 states that:

	> All XML processors MUST accept the UTF-8 and UTF-16 encodings of Unicode \[Unicode]; the mechanisms for signaling which of the two is in use, or for bringing other encodings into play, are discussed later, in 4.3.3 Character Encoding in Entities.

	Restricted XML only supports the UTF-8 encoding.

## Notes

The elimination of the `doctypedecl` production implies the removal of all non-predefined references. That means that a Restricted XML parser MUST NOT accept any entity references except `lt`, `amp`, `gt`, `quot`, `apos` (RX). Character references are unaffected by this.

   [XML]: https://www.w3.org/TR/REC-xml/
   [XML-NAMES]: https://www.w3.org/TR/REC-xml-names/

*/
