/*!
# Macros for XML strings

This crate provides macros to check XML string syntax at compile time.

## Example

```rust,ignore
use rxml::{NcNameStr, xml_cdata, xml_ncname};

const XML_PREFIX: &'static NcNameStr = xml_ncname!("xml");
const XML_QNAME: &'static NameStr = xml_name!("xml:lang");
```

## See also

This crate bases on the [`rxml_validation`] crate and it primarily intended
for use with the [`rxml`](https://docs.rs/rxml) crate.
*/
use proc_macro::TokenStream;
use quote::quote;
use rxml_validation::{validate_name, validate_ncname};
use syn::{LitStr, parse_macro_input};

/** XML 1.0 Name compliant string

# Example

```rust,ignore
use rxml::{NameStr, xml_name};

const FORBIDDEN: &'static NameStr = xml_name!("xmlns:xml");
*/
#[proc_macro]
pub fn xml_name(input: TokenStream) -> TokenStream {
	let data = parse_macro_input!(input as LitStr);
	let s = data.value();
	let tokens = match validate_name(&s) {
		Ok(()) => quote! { unsafe { std::mem::transmute::<_, &rxml::NameStr>(#s) } },
		Err(e) => {
			let err = format!("invalid Name string {:?}: {}", s, e);
			quote! { compile_error!(#err) }
		}
	};
	tokens.into()
}

/** Namespaces for XML 1.0 NCName compliant string

# Example

```rust,ignore
use rxml::{NcNameStr, xml_ncname};

const XML_PREFIX: &'static NCNameStr = xml_ncname!("xml");
*/
#[proc_macro]
pub fn xml_ncname(input: TokenStream) -> TokenStream {
	let data = parse_macro_input!(input as LitStr);
	let s = data.value();
	let tokens = match validate_ncname(&s) {
		Ok(()) => quote! { unsafe { std::mem::transmute::<_, &rxml::NcNameStr>(#s) } },
		Err(e) => {
			let err = format!("invalid NCName string {:?}: {}", s, e);
			quote! { compile_error!(#err) }
		}
	};
	tokens.into()
}
